import java.rmi.Naming;
 
public class CurrencyConverterClient {
 
    public static void main(String[] args) throws Exception{
        ICurrencyConverter server = (ICurrencyConverter)Naming.lookup("//localhost/CurrencyConverter");
        //System.out.println(server.getClass());
        System.out.println(server.convert("Gbp", "Usd", 10));
    }
 
}