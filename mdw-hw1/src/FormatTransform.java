import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.*;
import java.util.*;

public class FormatTransform {
	public static String FromData1ToData2(String data1) throws ParseException
	{
		String [] data1Parts = data1.split("\r\n");
		String name = data1Parts[2];
		String start = data1Parts[4];
		String location = data1Parts[6];
		String sumary = data1Parts[8];
		
		String when = StartToWhen(start);
		String[] locationParts = location.split(",");
		String street = locationParts[0];
		String city = locationParts[2];
		String zip = locationParts[1];
		
		String data2="{\r\n";
		data2 +="\t\"" + "what" + "\": \"" + name + " - " + sumary + "\",\r\n";
		data2 += "\t\"" + "when" +" \": \"" + when + "\",\r\n" ;
		data2 += "\t\"" + "where" + "\": {\r\n";
		data2 += "\t\t\"" + "street" + "\": \"" + street + "\",\r\n";
		data2 += "\t\t\"" + "city" + "\": \"" + city + "\",\r\n";
		data2 += "\t\t\"" + "zip" + "\": \"" + zip + "\"\r\n";
		data2 += "\t}\r\n";
		data2 += "}";
		
		
		return data2;
	}
	private static String StartToWhen(String date) throws ParseException
	{
		//System.out.println("input:"+date);
		
		Pattern pattern = Pattern.compile("(\\d{1,2})(st|nd|th)");
		Matcher m = pattern.matcher(date);
		m.find();
		date =  date.replaceAll(pattern.toString(), m.group(1));
		
		
		SimpleDateFormat sourceDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH);
		SimpleDateFormat targetDateFormat = new SimpleDateFormat("dd.M.yyyy");
		

		return targetDateFormat.format(sourceDateFormat.parse(date));
	}
}
