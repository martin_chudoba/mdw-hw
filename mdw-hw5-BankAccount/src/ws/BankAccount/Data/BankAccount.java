package ws.BankAccount.Data;

public class BankAccount {
	public BankAccount(int number, int balance)
	{
		accountNumber=number;
		this.balance=balance;
	}
	int accountNumber;
	int balance;
	public int GetBalance()
	{
		return balance;
	}
	public void SetBalance(int balance)
	{
		this.balance = balance;
	}
	public int GetAccountNumber()
	{
		return accountNumber;
	}
	public void SetAccountNumber(int accountNumber)
	{
		this.accountNumber=accountNumber;
	}
	
}
