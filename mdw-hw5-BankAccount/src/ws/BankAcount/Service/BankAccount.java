package ws.BankAcount.Service;

import java.util.ArrayList;
import java.util.List;

import javax.jws.*;




@WebService
public class BankAccount  {

	static List<ws.BankAccount.Data.BankAccount> db;
	static
	{
		db=new ArrayList<ws.BankAccount.Data.BankAccount>();
		db.add(new ws.BankAccount.Data.BankAccount(1,100));
		db.add(new ws.BankAccount.Data.BankAccount(2,100));
		db.add(new ws.BankAccount.Data.BankAccount(3,100));
		db.add(new ws.BankAccount.Data.BankAccount(4,100));
	}
	
	private ws.BankAccount.Data.BankAccount getAccountByNumber(int accountNumber)
	{
		for(ws.BankAccount.Data.BankAccount acc : db)
		{
			if(acc.GetAccountNumber()==accountNumber)
			{
				return acc;
			}
		}
		return null;
	}
	
	
	@WebMethod
	public boolean validateAccount(int accountNumber)
	{
		return getAccountByNumber(accountNumber)!=null?true:false;
	}
	@WebMethod
	public boolean validateBalance(int accountNumber, int amount)
	{
		ws.BankAccount.Data.BankAccount acc =getAccountByNumber(accountNumber);
		if(acc.GetBalance() > amount)
			return true;
		return false;
	}

	@WebMethod
	public void changeBalance(int accountNumber, int amount) {
		ws.BankAccount.Data.BankAccount acc = getAccountByNumber(accountNumber);
		acc.SetBalance(acc.GetBalance() + amount);
	}
}
