package ws.BankAcount.Service;

import javax.jws.*;
import javax.xml.ws.WebServiceRef;

import ws.bankacount.service.BankAccountService;



@WebService
public class BankAccountTransfer {
	//@WebServiceRef
	//private BankAccountService service;
	@WebMethod
	public boolean Transfer(int from, int to, int amount) {
		BankAccountService service = new BankAccountService();
		if(service.getBankAccountPort().validateAccount(from)==false)
				return false;
		if(service.getBankAccountPort().validateAccount(to)==false)
			return false;
		if(service.getBankAccountPort().validateBalance(from, amount)==false)
			return false;
		service.getBankAccountPort().changeBalance(from,-amount);
		service.getBankAccountPort().changeBalance(to, amount);
		return true;
	}
}
