

import java.io.IOException;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




/**
 * Servlet implementation class QueryJDNI
 */
public class QueryJDNI extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QueryJDNI() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Context ctx = null;
		 Connection conn = null;
		  Statement stmt = null;
		  ResultSet rs = null;
		  Hashtable ht = new Hashtable();
		  ht.put(Context.INITIAL_CONTEXT_FACTORY,
		         "weblogic.jndi.WLInitialContextFactory");
		  ht.put(Context.PROVIDER_URL,
		         "t3://localhost:7001");
		  

		    try {
				ctx = new InitialContext(ht);
		    javax.sql.DataSource ds 
		      = (javax.sql.DataSource) ctx.lookup ("mi-mdw-hw4-dataSource");
		    try {
				conn = ds.getConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		    
		    try {
			stmt = conn.createStatement();
		    stmt.execute("select * from test");
		    rs = stmt.getResultSet();
		    String res="";
		    while (rs.next()) {
	            res += rs.getString("test")+" "+rs.getString("foobar")+"\r\n" ;
		    	//System.out.println(rs.getString("test")+" "+rs.getString("foobar"));
	        }
		    stmt.close();
	        conn.close();
		    response.getWriter().println(res);
		    } catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   
	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
