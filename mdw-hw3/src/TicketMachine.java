

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class TicketMachine
 */
public class TicketMachine extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public enum ClientState {New, Payment, Completed}
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		HttpSession session = request.getSession();
		
		if(request.getCookies()==null)
		{
			response.getWriter().println("You need to set State cookie.");
			return;
		}
		ClientState state=null;
		for(Cookie c : request.getCookies())
		{
			if(c.getName().equals("State"))
			{
				state=ClientState.valueOf(c.getValue());
			}
		}
		if(state==null)
		{
			response.getWriter().println("You have to set State cookie.");
			return;
		}
		
		
		
		
		if(session.isNew())
		{
			if(state!=ClientState.New)
			{
				response.getWriter().println("You need to start in a New state.");
				session.invalidate();
				return;
			}
			else
			{
				session.setAttribute("state", ClientState.New);
			}
		}

		
		
		
		if(session.getAttribute("state")==ClientState.New)
		{
			if(state==ClientState.New)
			{
				response.getWriter().println("You are in a New state.");
			}
			else if(state == ClientState.Payment)
			{
				response.getWriter().println("You are in a Payment state.");
				session.setAttribute("state", ClientState.Payment);
			}
			else
			{
				response.getWriter().println("Wrong transition. Closing session.");
				session.invalidate();
			}
		}
		else if(session.getAttribute("state")==ClientState.Payment)
		{
			if(state==ClientState.Payment)
			{
				response.getWriter().println("You are in a Payment state.");
			}
			else if(state == ClientState.Completed)
			{
				response.getWriter().println("You are in a Completed state. Closing session.");
				session.invalidate();
			}
			else
			{
				response.getWriter().println("Wrong transition. Closing session.");
				session.invalidate();
			}
		}
	}
}
		
		
		
		
		 
		


