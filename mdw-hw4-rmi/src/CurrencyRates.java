import java.util.HashMap;


public class CurrencyRates {
	
	enum Currency{Eur, Usd, Gbp}
	
	public CurrencyRates()
	{
		rates.put(new Tuple(Currency.Eur, Currency.Usd), 1.249795);
		rates.put(new Tuple(Currency.Usd, Currency.Eur), 0.800131222);
		rates.put(new Tuple(Currency.Eur, Currency.Gbp), 0.782540229);
		rates.put(new Tuple(Currency.Gbp, Currency.Eur), 1.27788957);
		rates.put(new Tuple(Currency.Usd, Currency.Gbp), 0.626134869);
		rates.put(new Tuple(Currency.Gbp, Currency.Usd), 1.5971);
	}
	
	class Tuple
	{
		public Tuple(Currency from, Currency to)
		{
			From=from;
			To=to;
		}
		public Currency From;
		public Currency To;
		
		@Override
		public boolean equals(Object t)
		{
			if(t.getClass()==Tuple.class)
			{
			return From==((Tuple)t).From && To==((Tuple)t).To;
			}
			return super.equals(t);
		}
		@Override
		public int hashCode()
		{
			return Integer.parseInt(Integer.toString(From.ordinal()) + Integer.toString(To.ordinal()));
		}
	}
	
	private HashMap<Tuple, Double> rates = new HashMap<Tuple, Double>();
	
	public double getRate(Currency from, Currency to)
	{
		return rates.get(new Tuple(from, to));
	}
}
