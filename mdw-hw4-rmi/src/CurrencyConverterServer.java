import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
 
public class CurrencyConverterServer extends UnicastRemoteObject implements ICurrencyConverter{
	   

	protected CurrencyConverterServer() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}
	private static final long serialVersionUID = 6613593458917190489L;
	
	CurrencyRates rates = new CurrencyRates();
	
	public double convert(String from, String to, double amount)
	{
		return convert(CurrencyRates.Currency.valueOf(from),CurrencyRates.Currency.valueOf(to) ,amount);
	}
	private double convert(CurrencyRates.Currency from, CurrencyRates.Currency to, double amount)
	{
		return rates.getRate(from, to)*amount;
	}
	public static void main(String[] args) {
	        try {
	            LocateRegistry.createRegistry(1099); 
	 
	            CurrencyConverterServer server = new CurrencyConverterServer();
	            Naming.rebind("//0.0.0.0/CurrencyConverter", server);
	 
	            System.out.println("Server started...");
	 
	        } catch (Exception e) {
	            System.out.println("Error: " + e.getLocalizedMessage());
	        }
	 
	    }
}
