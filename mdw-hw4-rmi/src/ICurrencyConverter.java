import java.rmi.Remote;
import java.rmi.RemoteException;


public interface ICurrencyConverter extends Remote{
	
	double convert(String from, String to, double amount) throws RemoteException;;
}
